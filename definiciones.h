#ifndef _DEFINICIONES_H_
#define _DEFINICIONES_H_

unsigned const int AUTOMATICO = 1; 			// MODO : AUTOM�TICO / MANUAL.
unsigned const int MANUAL = 0;						
		
unsigned const int METROS = 1;					// ESCALA : METROS / CENT�METROS.
unsigned const int CENTIMETROS = 0;			

unsigned const int ENCENDIDO = 1;				// MEDIDOR : ENCENDIDO / APAGADO.
unsigned const int APAGADO = 0;

unsigned const int ON = 1;							// ESTADO: ON / OFF.
unsigned const int OFF = 0;

unsigned const char array[10] = {0xC0,0xF9,0xA4,0xB0,0x99,0x92,0x82,0xF8,0x80,0x90};	//ARRAY DE N�MEROS EN HEX (0-9)

#endif
