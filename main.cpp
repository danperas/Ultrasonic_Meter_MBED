/**
*	Pr�ctica 3 Fase 2:
*
*	Programa que simula el encendido de un medidor, as� como una serie de opciones entre las que el usuario puede
*	alternar a su gusto seg�n sus necesidades. Obviamente, las limitaciones del programa son acordes a las instrucciones
*	dadas en Moodle.
*
* Se ha modificado el tiempo de muestreo en DebounceIn.h para mayor comodidad al pulsar, aumentando 1ms a 5ms.
*
*	@author Daniel P�rez Asensio & Carlos Escudero Tabernero
*	@date 31-3-2016
*/
#include "mbed.h"																								//Importaci�n de librer�as necesarias.
#include "definiciones.h"
#include "hardware.h"
#include "funciones.h"

void encendido();
void modoAutomatico();
/**
*	Funci�n principal Main.
*/
int main() {																										// Medidor apagado.	

	unsigned int estadoMedir = APAGADO;														// Guarda el estado del medidor, inicialmente APAGADO.
	unsigned int estadoModo = MANUAL;															// Guarda el estado del modo, inicialmente en MANUAL.
	unsigned int estadoEscala = CENTIMETROS;											// Guarda el estado de la escala, inicialmente en CENTIMETROS.
	
	medir.mode(PullDown);																					// Se utiliza la funci�n mode(PullDown) para no usar una resistencia de pull down
	modo.mode(PullDown);																					// f�sica en el circuito. 
	escala.mode(PullDown);
	wait(0.001);																									// Delay para que el PullDown se inicialice.

	while(estadoMedir == APAGADO){																// Mientras no se pulsa MEDIR, el medidor no se enciende.
		if (medir){																									// Se enciende el medidor.
			encendido();
			estadoMedir = ENCENDIDO;
		}
		while(medir){}																							// Mientras est� pulsado el bot�n, no ocurre nada.
	}
	while (1){																										// El medidor se encuentra encendido.

		if (modo && estadoModo == AUTOMATICO){											// Si se pulsa MODO y el modo estaba en AUTOM�TICO, pasa a estar en MANUAL.
			modoLED.write(0);
			estadoModo = MANUAL;
			while(modo){}
		}
		else if (modo && estadoModo == MANUAL){											// Si se pulsa MODO y el modo estaba en MANUAL, pasa a estar en AUTOM�TICO.
			modoLED.write(1);
			estadoModo = AUTOMATICO;
			while(modo){}
		}else{}
			
		if (estadoModo == AUTOMATICO && medir){											// Si se pulsa MEDIR y el modo estaba en AUTOM�TICO, entra en medici�n autom�tica.
			estadoMedir = 0;
			while(estadoMedir == OFF){																// Mientras no se pulse de nuevo MEDIR, no saldr� de medici�n autom�tica.
					midiendoLED.write(1);
					wait(0.25);
					if (estadoMedir == OFF)																// Si se pulsa MEDIR, sale de la medici�n autom�tica.
						estadoMedir = medir.read();
					midiendoLED.write(0);
					wait(0.75);
					if (estadoMedir == OFF)
						estadoMedir = medir.read();
			}
			while(medir){}
		}
		if (escala && estadoEscala == CENTIMETROS){									// Si se pulsa ESCALA y escala estaba en CENTIMETROS, pasa a estar en METROS.
			escalaLED.write(1);
			estadoEscala = METROS;
			while(escala){}
		}
		else if (escala && estadoEscala == METROS){									// Si se pulsa ESCALA Y escala estaba en METROS, pasa a estar en CENTIMETROS.
			escalaLED.write(0);
			estadoEscala = CENTIMETROS;
			while(escala){}
		}
		if (estadoModo == MANUAL && medir) {												// Si el modo est� en manual y se pulsa MEDIR, se hace una �nica medici�n.
			midiendoLED.write(1);
			wait(0.25);
			midiendoLED.write(0);
			while(medir){}
		}		
	}
}
