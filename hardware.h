#include "mbed.h"
#include "DebounceIn.h"

#ifndef _HARDWARE_H_
#define _HARDWARE_H_

extern BusOut display;									// Segmentos a,b,c,d,e,f,g (dp no se ha conectado).

extern DigitalOut display1, display2, midiendoLED, modoLED, escalaLED;	// Pines que actuar�n en modo escritura.

extern DebounceIn medir, modo, escala;		// Pines que actuar�n en modo lectura y que evitar�n el rebote inicial del pulsador.
																					// La funci�n DebounceIn toma 10 muestras en 1ms.

#endif